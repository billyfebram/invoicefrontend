import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { InvoiceFrontendSharedLibsModule, InvoiceFrontendSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [InvoiceFrontendSharedLibsModule, InvoiceFrontendSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [InvoiceFrontendSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class InvoiceFrontendSharedModule {
  static forRoot() {
    return {
      ngModule: InvoiceFrontendSharedModule
    };
  }
}
