import { NgModule } from '@angular/core';

import { InvoiceFrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [InvoiceFrontendSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [InvoiceFrontendSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class InvoiceFrontendSharedCommonModule {}
